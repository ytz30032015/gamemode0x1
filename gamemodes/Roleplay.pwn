// This is a comment
// uncomment the line below if you want to write a filterscript
//#define FILTERSCRIPT

#include <a_samp>
#include <animsFix>

#include <crashdetect>
#include <a_mysql>
#include <streamer>
#include <zcmd>
#include <sscanf2>
#include <easyDialog>

// DEFINITIONS //
#define SQL_SERVER  "127.0.0.1"
#define SQL_USER    "root"
#define SQL_DB      "ZumeSAMP"
#define SQL_PASS    ""
#define SQL_DEBUG	1 // 0 - disable , 1 - enable MySQL debug.

// MACROS
#define PlayerLogged(%0) ((PlayerInfo[playerid][playerLogged] && IsPlayerConnected(playerid)) ? (true) : (false))
#define GetPlayerTalkStyle(%1) (PlayerInfo[%1][playerTalkType])
#define GetPlayerTimeCommand(%1) (PlayerInfo[%1][playerLastTimeCMD])
#define ConvertSeconds(%1) (%1-gettime())
#define SetPlayerTimeCommand(%1,%0) PlayerInfo[%1][playerLastTimeCMD] = gettime()+%0

// THREADS
#define THREAD_CHECK_ACCOUNT    1
#define THREAD_CHECK_PASS       2
#define THREAD_LOAD_DATA        3

// OTHER //
native WP_Hash(buffer[], len, const str[]);

// ENUMS //
enum pInfo
{
	playerLastTimeCMD,
	playerLogged,

	// DATOS GUARDADOS //
	playerID,
	playerMoney,
	playerAdmin,
	playerSkin,
	playerINT,
	playerVW,
	playerTalkType,
	Float:playerPos[4]
};
new PlayerInfo[MAX_PLAYERS][pInfo],
gSQLHandle;

enum eTalkStyles
{
	talkName[32],
	talkAnim[20],
	talkLib[16]
};
static const TalkStyles[][eTalkStyles] =
{
	{
		"Normal", 		"IDLE_CHAT", 	"ped"},
	{
		"Alzado", 		"Idle_Chat_02", "MISC"},
	{
		"Malandro 1", 	"gsign4LH", 	"GHANDS"},
	{
		"Malandro 2", 	"gsign5", 		"GHANDS"},
	{
		"Malandro 3", 	"gsign4", 		"GHANDS"},
	{
		"Malandro 4", 	"gsign3", 		"GHANDS"},
	{
		"Malandro 5", 	"gsign1LH", 	"GHANDS"}
};

main()
{

}

public OnGameModeInit()
{
	gSQLHandle = mysql_connect(SQL_SERVER, SQL_USER, SQL_DB, SQL_PASS);

	if(mysql_errno(gSQLHandle) != 0){
		return print("La conexion a la base de datos fallo!");
	}
	else{
		print("La conexion a la base de datos se realizo!");
	}

	mysql_log();

	UsePlayerPedAnims();
	ShowPlayerMarkers(0);
	DisableInteriorEnterExits();
	EnableStuntBonusForAll(0);

	// Don't use these lines if it's a filterscript
	SetGameModeText("Blank Script");
	//AddPlayerClass(0, 1958.3783, 1343.1572, 15.3746, 269.1425, 0, 0, 0, 0, 0, 0);
	return 1;
}

public OnPlayerConnect(playerid)
{
	SetPlayerColor(playerid, 0xFFFFFFFF);

	SetPlayerHealth(playerid, 100.0);
	SetPlayerArmour(playerid, 0.0);
	ResetPlayerData(playerid);
	return 1;
}

public OnPlayerSpawn(playerid)
{
	if(!IsPlayerConnected(playerid))
	return 0;

	SetPlayerPos(playerid, PlayerInfo[playerid][playerPos][0], PlayerInfo[playerid][playerPos][1], PlayerInfo[playerid][playerPos][2]);
	SetPlayerFacingAngle(playerid, PlayerInfo[playerid][playerPos][3]);
	SetPlayerVirtualWorld(playerid, PlayerInfo[playerid][playerVW]);
	SetPlayerInterior(playerid, PlayerInfo[playerid][playerINT]);
	SetPlayerSkin(playerid, PlayerInfo[playerid][playerSkin]);
	return 1;
}

public OnPlayerRequestClass(playerid, classid)
{
	if(IsPlayerNPC(playerid))
	return 1;

	if(PlayerLogged(playerid))
	{
		
		SpawnPlayer(playerid);
	}
	else
	{
		
		TogglePlayerSpectating(playerid, true);

		SetSpawnInfo(playerid, 0, 0, 1958.33, 1343.12, 15.36, 269.15, -1, -1, -1, -1, -1, -1);
		TogglePlayerControllable(playerid, false);
		SendToLogin(playerid);
	}
	return 1;
}

public OnGameModeExit()
{
	return 1;
}

public OnPlayerDisconnect(playerid, reason)
{
	SaveAccount(playerid);
	return 1;
}

public OnPlayerText(playerid, text[])
{
	if(PlayerInfo[playerid][playerAdmin] > 0)
	{
		
		// Devolper Debug Mode
		if(text[0] == '!')
		{
			
			if(strcmpEx(text[1], "pos"))
			{
				
				new
				Float:playerPosTemp[4];

				GetPlayerPos(playerid, playerPosTemp[0], playerPosTemp[1], playerPosTemp[2]);
				GetPlayerFacingAngle(playerid, playerPosTemp[3]);
				printf("[Print->PlayerPos] %.2f, %.2f, %.2f, %.2f", playerPosTemp[0], playerPosTemp[1], playerPosTemp[2], playerPosTemp[3]);
				return 0;
			}
		}
	}
	if(GetPlayerState(playerid) == PLAYER_STATE_ONFOOT)
	{
		
		if(strlen(text) > 0)
		{
			
			new
			talk_style = GetPlayerTalkStyle(playerid);

			ApplyAnimation(playerid, TalkStyles[talk_style][talkLib], TalkStyles[talk_style][talkAnim], 4.1, 1, 1, 1, 1, strlen(text) * 100, 1);
			SetTimerEx("StopChatting", strlen(text) * 100, false, "d", playerid);
		}
	}
	SetPlayerChatBubble(playerid, text, 0xFFFFFF, 20.0, 6000);
	return 1;
}

// DIALOGS //

Dialog:LoginAccount(playerid, response, listitem, inputtext[])
{
	if(!response)
	return Kick(playerid);

	if(inputtext[0] == '\0')
	return Kick(playerid);

	if(!IsPlayerConnected(playerid) || PlayerLogged(playerid))
	return 0;

	new
	query[160]
	;

	format(query, sizeof(query), "SELECT `id` FROM `users` WHERE `name` = '%s' AND `pass` = '%s'", _getNameEx(playerid), inputtext);
	mysql_function_query(gSQLHandle, query, true, "QueryCheckAccount", "dd", playerid, THREAD_CHECK_PASS);
	return 1;
}

Dialog:RegisterAccount(playerid, response, listitem, inputtext[])
{
	if(!response)
	return Kick(playerid);

	if(inputtext[0] == '\0')
	return Kick(playerid);

	SQL_CreateAccount(playerid, _getNameEx(playerid), inputtext);
	SendLoadPlayerAccount(playerid);
	return 1;
}

Dialog:TalkMode(playerid, response, listitem, inputtext[])
{
	if(!response)
	return 1;

	PlayerInfo[playerid][playerTalkType] = listitem;
	SendClientMessageEx(playerid, -1, "Tu estilo al hablar es ahora %s", TalkStyles[listitem][talkName]);
	return 1;
}

/////////////////////////

// COMMANDS //

CMD:estilohablar(playerid, params[])
{
	new
	str[128];

	for(new i; i != sizeof(TalkStyles); i++)
	{
		
		format(str, sizeof(str), "%s%s\n", str, TalkStyles[i][talkName]);
	}
	Dialog_Show(playerid, TalkMode, DIALOG_STYLE_LIST, "Estilo de hablar [Manos]", str, "Seleccionar", "Cancelar");
	return 1;
}

CMD:anim(playerid, params[])
{
	new anim[32], lib[32];
	if(sscanf(params, "s[32]s[32]", lib, anim))
	return 1;

	ApplyAnimation(playerid, lib, anim, 4.0, 1, 1, 1, 0, 1000);
	return 1;
}

/////////////////////////

forward OnPlayerCommandReceived(playerid, cmdtext[]);
public OnPlayerCommandReceived(playerid, cmdtext[])
{
	if(!IsPlayerConnected(playerid))
	return 0;

	new
	time_command = GetPlayerTimeCommand(playerid);

	if(gettime() < time_command)
	return SendClientMessageEx(playerid, -1, "* Espera %d segundos.", ConvertSeconds(time_command)), 0;

	SetPlayerTimeCommand(playerid, 1);
	return 1;
}

stock SQL_CreateAccount(playerid, username[], password[])
{
	new query[129+120]
	;

	format(query, sizeof(query), "INSERT INTO `users` (`name`, `pass`) VALUES('%s', '%s')", username, password);
	mysql_function_query(gSQLHandle, query, false, "CrearCuenta", "d", playerid);
}

forward CrearCuenta(playerid); public  CrearCuenta(playerid)
{
	PlayerInfo[playerid][playerID] = cache_insert_id(gSQLHandle);
	return 1;
}

SendToLogin(playerid)
{
	if(playerid == INVALID_PLAYER_ID)
	return 0;

	new
	query[120]
	;

	format(query, sizeof(query), "SELECT id, name FROM `users` WHERE `name` = '%s' LIMIT 1", _getNameEx(playerid));
	mysql_function_query(gSQLHandle, query, true, "QueryCheckAccount", "dd", playerid, THREAD_CHECK_ACCOUNT); 
	return 1;
}

stock SendLoadPlayerAccount(playerid)
{
	new
	query[160];

	format(query, sizeof(query), "SELECT * FROM `users` WHERE `name` = '%s'", _getNameEx(playerid));
	mysql_function_query(gSQLHandle, query, true, "QueryCheckAccount", "dd", playerid, THREAD_LOAD_DATA);
	return 1;
}

stock _getNameEx(playerid)
{
	new
	name[MAX_PLAYER_NAME + 1]
	;

	GetPlayerName(playerid, name, sizeof(name));
	return name;
}

cache_get_field_int(row, const field_name[])
{
	new
	str[12]
	;

	cache_get_field_content(row, field_name, str, gSQLHandle);
	return strval(str);
}

stock Float:cache_get_field_float(row, const field_name[])
{
	new
	str[16]
	;

	cache_get_field_content(row, field_name, str, gSQLHandle);
	return floatstr(str);
}

forward QueryCheckAccount(playerid, option); public QueryCheckAccount(playerid, option)
{
	new rows, fields;

	if(playerid == INVALID_PLAYER_ID)
	return 0;

	switch(option)
	{
		
		case THREAD_CHECK_ACCOUNT:
		{
			
			cache_get_data(rows, fields, gSQLHandle);
			if(rows)
			{
				
				SendClientMessage(playerid, -1, "Ingresa tu Password para identificarte");
				PlayerInfo[playerid][playerID] = cache_get_field_int(0, "id"); 
				Dialog_Show(playerid, LoginAccount, DIALOG_STYLE_PASSWORD, "Inicio", "Ingresa tu pass para identificarte ->", "Aceptar", "Salir");
			}
			else
			{
				
				Dialog_Show(playerid, RegisterAccount, DIALOG_STYLE_PASSWORD, "Registro", "Ingresa tu pass para registrarte ->", "Aceptar", "Salir");
			}
			return 1;
		}
		case THREAD_CHECK_PASS:
		{
			
			cache_get_data(rows, fields, gSQLHandle);
			if (!rows) 
			return Kick(playerid);

			SendLoadPlayerAccount(playerid);
			return 1;
		}
		case THREAD_LOAD_DATA:
		{
			
			cache_get_data(rows, fields, gSQLHandle);
			if (!rows)
			return Kick(playerid);

			PlayerInfo[playerid][playerLogged] = true;
			PlayerInfo[playerid][playerINT] = cache_get_field_int(0, "interior");
			PlayerInfo[playerid][playerVW] = cache_get_field_int(0, "vw");
			PlayerInfo[playerid][playerSkin] = cache_get_field_int(0, "skin");
			PlayerInfo[playerid][playerPos][0] = cache_get_field_float(0, "x");
			PlayerInfo[playerid][playerPos][1] = cache_get_field_float(0, "y");
			PlayerInfo[playerid][playerPos][2] = cache_get_field_float(0, "z");
			PlayerInfo[playerid][playerPos][3] = cache_get_field_float(0, "a");

			SendClientMessage(playerid, -1, "Bienvenido al servidor!");
			SpawnPlayerEx(playerid);
			return 1;
		}
	}
	return 0;
}

stock SpawnPlayerEx(playerid)
{
	if(!IsPlayerConnected(playerid))
	return 0;

	TogglePlayerSpectating(playerid, false);
	return 1;
}

stock SaveAccount(playerid)
{
	if(!PlayerLogged(playerid))
	return 0;

	new
	query[2400]
	;

	if(GetPlayerState(playerid) != PLAYER_STATE_SPECTATING)
	{
		
		PlayerInfo[playerid][playerINT] = GetPlayerInterior(playerid);
		PlayerInfo[playerid][playerVW] = GetPlayerVirtualWorld(playerid);
		PlayerInfo[playerid][playerSkin] = GetPlayerSkin(playerid);

		GetPlayerPos(playerid,PlayerInfo[playerid][playerPos][0],PlayerInfo[playerid][playerPos][1],PlayerInfo[playerid][playerPos][2]);
		GetPlayerFacingAngle(playerid, PlayerInfo[playerid][playerPos][3]);
	}

	format(query, sizeof(query), "UPDATE `users` SET `interior` = '%d', `vw` = '%d', `skin` = '%d'",
	PlayerInfo[playerid][playerINT],
	PlayerInfo[playerid][playerVW],
	PlayerInfo[playerid][playerSkin]
	);

	format(query, sizeof(query), "%s, `x` = '%.4f', `y` = '%.4f', `z` = '%.4f', `a` = '%.4f' WHERE `id` = '%d'",
	query,
	PlayerInfo[playerid][playerPos][0],
	PlayerInfo[playerid][playerPos][1],
	PlayerInfo[playerid][playerPos][2],
	PlayerInfo[playerid][playerPos][3],
	PlayerInfo[playerid][playerID]
	);
	mysql_function_query(gSQLHandle, query, false, "", "");
	return 1;
}

forward StopChatting(playerid); public StopChatting(playerid)
{
	ApplyAnimation(playerid, "CARRY", "crry_prtial", 4.0, 0, 0, 0, 0, 0);
}

stock ResetPlayerData(playerid)
{
	for(new pInfo:i; i < pInfo; i++)
	{
		
		printf("-- %d", PlayerInfo[playerid][i]);
		PlayerInfo[playerid][i] = 0;
		printf("--- %d", PlayerInfo[playerid][i]);
	}
	return 1;
}

stock GetPlayerIpEx(playerid)
{
	new
	playerIP[16]
	;

	GetPlayerIp(playerid, playerIP, sizeof(playerIP));
	return playerIP;
}

stock GetPlayerNameEx(playerid, type=0)
{
	new name[MAX_PLAYER_NAME];
	GetPlayerName(playerid, name, MAX_PLAYER_NAME);
	if(type == 1)
	{
		
		for(new i = 0; i < sizeof(name); i++)
		{
			
			if(name[i] == '_')
			{
				
				name[i] = ' ';
			}
		}
	}
	return name;
}

stock strcmpEx(cmp[], cmp2[])
{
	if(!strcmp(cmp, cmp2, true)) return 1;
	else return 0;
}

stock SendClientMessageEx(playerid, color, const str[], {Float,_}:...)
{
	static
	args,
	start,
	end,
	string[144]
	;
	#emit LOAD.S.pri 8
	#emit STOR.pri args

	if (args > 12)
	{
		
		#emit ADDR.pri str
		#emit STOR.pri start

		for (end = start + (args - 12); end > start; end -= 4)
		{
			
			#emit LREF.pri end
			#emit PUSH.pri
		}
		#emit PUSH.S str
		#emit PUSH.C 144
		#emit PUSH.C string
		#emit PUSH.C args
		#emit SYSREQ.C format

		SendClientMessage(playerid, color, string);

		#emit LCTRL 5
		#emit SCTRL 4
		#emit RETN
	}
	return SendClientMessage(playerid, color, str);
}
